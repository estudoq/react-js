#!/bin/bash
stripeSecretKey=#chave secreta da api do stripe

echo "Enter the port to host: "
read port

echo "Starting Docker container..."

docker run --rm -it stripe/stripe-cli listen --forward-to http://$(ip -o route get to 8.8.8.8 | sed -n 's/.*src \([0-9.]\+\).*/\1/p'):$port/api/webhooks \
    --api-key $stripeSecretKey
