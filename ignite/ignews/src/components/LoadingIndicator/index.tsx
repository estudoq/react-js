import { Digital } from "react-activity";
import "react-activity/dist/Digital.css";

interface LoadingIndicatorProps {
    color: string;
    isLoading: boolean;
}

export function LoadingIndicator({
    color,
    isLoading
}: LoadingIndicatorProps) {
    return (
        <span>
            <Digital color={color} animating={isLoading} />
        </span>
    );
}