import { useState } from 'react';
import { signIn, signOut, useSession } from 'next-auth/react';
import { FaGithub } from 'react-icons/fa';
import { FiX } from 'react-icons/fi';

import styles from './styles.module.scss';
import Swal from 'sweetalert2';
import { LoadingIndicator } from '../LoadingIndicator';

export function SignInButton() {
    const { data } = useSession();
    const [isLoading, setIsLoading] = useState(false);

    async function handleSignIn(){
        setIsLoading(true);

        try {
            await signIn('github');
            setIsLoading(false);
        } catch (err) {
            setIsLoading(false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: err.message,
            });
        }
    }

    async function handleSignOut(){
        setIsLoading(true);

        try {
            await signOut();
            setIsLoading(false);
        } catch (err) {
            setIsLoading(false);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: err.message,
            });
        }
    }

    return data ? (
        <button 
            type="button"
            className={styles.signInButton}
            onClick={handleSignOut}
        >
            <FaGithub color="#04d361" />
            {isLoading ? <LoadingIndicator color="#fff" isLoading={isLoading} /> : data.user.name}
            <FiX color="#737380" className={styles.closeIcon} />
        </button>
    ) : (
        <button 
            type="button"
            className={styles.signInButton}
            onClick={handleSignIn}
        >
            <FaGithub color="#eba417" />
            {isLoading ? <LoadingIndicator color="#fff" isLoading={isLoading} /> : "Sign in with Github" }
        </button>
    );
}