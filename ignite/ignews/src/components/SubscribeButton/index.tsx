import { signIn, useSession } from 'next-auth/react';
import Swal from 'sweetalert2';

import { api } from '../../services/api';
import { getStripeJs } from '../../services/stripe-js';

import styles from './styles.module.scss';
import { useState } from 'react';
import { LoadingIndicator } from '../LoadingIndicator';

interface SubscribeButtonProps {
    priceId: string;
}

export function SubscribeButton({ priceId }: SubscribeButtonProps) {
    const { data: session } = useSession();
    const [isLoading, setIsLoading] = useState(false);
    
    async function handleSubscribe() {
        setIsLoading(true);

        if(!session) {
            signIn('github');
            setIsLoading(false);
            return;
        }

        try {
            const response = await api.post('/subscribe')

            const { sessionId } = response.data;

            const stripe = await getStripeJs();

            await stripe.redirectToCheckout({ sessionId });

            setIsLoading(false)
        } catch (error) {
            
            setIsLoading(false);

            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: error.message,
            });
        }
    }

    return (
        <>
        <button
            type="button"
            className={styles.subscribeButton}
            onClick={handleSubscribe}
        >
            {isLoading ? <LoadingIndicator color="#121214" isLoading={isLoading} /> : "Subscribe now"}
        </button>
        </>
    );
}